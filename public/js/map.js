window.addEventListener('load', function () {
  let map;
  let kmlParser;
  const kmlSrc = this.localStorage.getItem('kmlSrc');
  const src = kmlSrc
  const zoneNames = []
  const latLng = { lat: 50.1909938097998, lng: 21.4837594528992 };

  map = new google.maps.Map(document.getElementById('map'), {
    center: latLng,
    mapTypeId: 'satellite',
  });

  kmlParser = new geoXML3.parser({
    map: map,
    suppressInfoWindows: true,
    preserveViewport: true,
    afterParse: setupKml,
  })

  kmlParser.parse(src)

  function setupKml(docs) {
    const placemarks = docs[0].placemarks ?? []
    const polygons = []
    let coordinates = [];
    const zoneNumberElement = document.querySelector('.zone-number')
    
    // Push all field coordinates to later find the biggest zone
    placemarks.forEach(placemark => {
      const placemarkFieldCoords = placemark.Polygon[0].outerBoundaryIs[0].coordinates
      coordinates.push(placemarkFieldCoords)
    })
  
    // Create field polygons with biggest zone highlighted
    placemarks.forEach((placemark) => {
      const placemarkFieldCoords = placemark.Polygon[0].outerBoundaryIs[0].coordinates
  
      let fieldPolygon

      if(localStorage.getItem('zone-number') !== null && placemark.name === localStorage.getItem('zone-number')) {
        placemark.showsZone = true
        fieldPolygon = new google.maps.Polygon({
          paths: placemarkFieldCoords,
          strokeColor: "#FF0000",
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: "#000AEB",
          fillOpacity: 0.25,
        })
  
        fieldPolygon.name = placemark.name
        fieldPolygon.showsZone = true
        
        zoneNames.push(placemark.name)
        polygons.push(fieldPolygon)
        if(zoneNumberElement) zoneNumberElement.innerText = localStorage.getItem('zone-number')
        localStorage.setItem("zone-number", placemark.name)

      } else if(placemarkFieldCoords === getBiggestZone(coordinates) && localStorage.getItem('zone-number') === null) {
        placemark.showsZone = true
        fieldPolygon = new google.maps.Polygon({
          paths: placemarkFieldCoords,
          strokeColor: "#FF0000",
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: "#000AEB",
          fillOpacity: 0.25,
        })
  
        fieldPolygon.name = placemark.name
        fieldPolygon.showsZone = true
  
        zoneNames.push(placemark.name)
        polygons.push(fieldPolygon)
  
        if(zoneNumberElement) zoneNumberElement.innerText = placemark.name
      } else {
        placemark.showsZone = false
        fieldPolygon = new google.maps.Polygon({
          paths: placemarkFieldCoords,
          strokeColor: "#FF0000",
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: "#FF0000",
          fillOpacity: 0.25,
        })
  
        fieldPolygon.name = placemark.name
        fieldPolygon.showsZone = false
  
        zoneNames.push(placemark.name)
        polygons.push(fieldPolygon)
      }
  
      google.maps.event.addListener(fieldPolygon, "mouseover", function() {
        if(this.fillColor !== "#000AEB") {
          this.setOptions({fillOpacity: 0.4})
        }
        
      })
      google.maps.event.addListener(fieldPolygon, "mouseout", function() {
        this.setOptions({fillOpacity: 0.25})
      })
  
      google.maps.event.addListener(fieldPolygon, "click", function() {
        polygons.forEach(polygon => {
          if(polygon.showsZone === true) {
            polygon.showsZone = false
            polygon.setOptions({fillColor: "#FF0000"})
          }
        })
  
        if(fieldPolygon.showsZone === false) {
          fieldPolygon.showsZone = true
          fieldPolygon.setOptions({fillColor: "#000AEB"})
        }
  
        if(zoneNumberElement) zoneNumberElement.innerText = placemark.name
        localStorage.setItem("zone-number", placemark.name)
      });
    
      fieldPolygon.setMap(map)
    })
  }
  
  function getBiggestZone(coordinates) {
    let highlightedCoords = []
    for(let i = 0; i < coordinates.length; i++) {
      if(highlightedCoords.length < coordinates[i].length) {
        highlightedCoords = coordinates[i]
      }
    }
  
    return highlightedCoords
  }
})
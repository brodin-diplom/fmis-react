// export const fieldsEndpoint = "http://localhost:4000/api/fields"
// export const fieldsEndpointWithId = (id) => `http://localhost:4000/api/fields/${id}`
// export const usersEndpoint = "http://localhost:4000/api/users"
// export const usersEndpointWithId = (id) => `http://localhost:4000/api/users/${id}`
// export const simulationsEndpoint = "http://localhost:4000/api/simulations"
// export const simulationsEndpointWithId = (id) => `http://localhost:4000/api/simulations/${id}`
// export const eventTypesEndpoint = "http://localhost:4000/api/EventType"


export const fieldsEndpoint = "https://fmis-manager-hjnkolnyrq-oa.a.run.app/api/fields"
export const fieldsEndpointWithId = (id) => `https://fmis-manager-hjnkolnyrq-oa.a.run.app/api/fields/${id}`
export const usersEndpoint = "https://fmis-manager-hjnkolnyrq-oa.a.run.app/api/users"
export const usersEndpointWithId = (id) => `https://fmis-manager-hjnkolnyrq-oa.a.run.app/api/users/${id}`
export const simulationsEndpoint = "https://fmis-manager-hjnkolnyrq-oa.a.run.app/api/simulations"
export const simulationsEndpointWithId = (id) => `https://fmis-manager-hjnkolnyrq-oa.a.run.app/api/simulations/${id}`
export const eventTypesEndpoint = "https://fmis-manager-hjnkolnyrq-oa.a.run.app/api/EventType"
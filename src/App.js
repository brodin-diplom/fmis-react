import './App.css';
import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import Home from './routes/Home/Home'
import Manage from './routes/Manage/Manage'
import CreateManagementFile from './routes/CreateManagementFile/CreateManagementFile'
import Fields from './routes/Fields/Fields'
import Simulations from './routes/Simulations/Simulations'

import { configs } from './firebase.config'

import firebase from 'firebase/app'
import 'firebase/auth'

firebase.initializeApp(configs)

const App = () => {
  
    return (
      <Router>
        <Switch>
          <Route path="/manage">
            <Manage />
          </Route>
          <Route path="/fields">
            <Fields />
          </Route>
          <Route path="/simulations">
            <Simulations />
          </Route>
          <Route path="/management-file-generator">
            <CreateManagementFile />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Router>
    );
}

export default App;

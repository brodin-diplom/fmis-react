import React, { useEffect, useState } from "react"
import styles from "./Fields.module.css"
import Map from "./../../components/Map/Map"
import FieldList from "./../../components/FieldList/FieldList"
import MenuBar from './../../components/MenuBar/MenuBar'
import { isSignedIn } from './../../services'
import { NavLink, useHistory } from 'react-router-dom'

import { get, getUid } from '../../services'
import { fieldsEndpoint } from '../../endpoints'

const Fields = () => {
  const [fields, setFields] = useState(null)
  const history = useHistory()

  if(!isSignedIn()) {
    history.replace('/')
  }

  useEffect( () => {
    get(fieldsEndpoint+`?uid=${getUid()}`).then(fields => {
      if(fields.length !== 0) {
        if(localStorage.getItem('fieldIndex') === null) localStorage.setItem('fieldIndex', 0)
        if(localStorage.getItem('fieldId') === null) localStorage.setItem('fieldId', fields[0].id)
        if(localStorage.getItem('kmlSrc') === null) localStorage.setItem('kmlSrc', fields[0].kmlUrl)
        if(localStorage.getItem('zone-number') === null) localStorage.setItem('zone-number', fields[0].zones[0].id)
      }
    setFields(fields)
    })
  }, [])
  
  return (
    <div className={styles.Fields}>
      <MenuBar />
        <div className="container-fluid">
          <div className="row">
            { fields !== null && fields.length !== 0 &&
              <div className="col-xs-12 col-lg-2">
                <FieldList fields={fields}/>
              </div>
            }
            <div className="col-xs-12 col-lg-9 col-lg-offset-1">
              <Map/>
            </div>
          </div>
          { fields !== null && fields.length === 0 &&
            <div style={{"position": "fixed", "top": "0", "left": "0", "width": "100vw", "height" : "100vh", "background": "white"}}  className="row center-xs">
              <div>
                <h2 style={{"marginTop": "30%",}}>You don't have any fields registered yet...</h2>
                <div className="col-xs-12">
                  <NavLink to="/manage">
                  <button style={{"marginTop": "10px", "marginLeft": "5px"}} className="button-primary">Register field</button>
                  </NavLink>
                </div>
              </div>
            </div>
          }
        </div>
      
    </div>
  )
}

export default Fields;
import React, { useState } from 'react';
import styles from './Manage.module.css'

import MenuBar from '../../components/MenuBar/MenuBar'
import UploadPopup from '../../components/UploadPopup/UploadPopup'
import { NavLink, useHistory } from "react-router-dom"
import { isSignedIn } from '../../services'

const Manage = () => {

    const [showPopup, setShowPopup] = useState(false)
    const history = useHistory()

    if(!isSignedIn()) {
      history.replace('/')
    }

    const handleUploadDocument = () => setShowPopup(!showPopup)

    const handleDismiss = () => setShowPopup(false)

    return(
      <div className={styles.Manage}>
        <MenuBar />
        <UploadPopup
          dismiss = {handleDismiss}
          shouldShowPopup = {showPopup}
        />
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <h1 className="title">What would you like to do?</h1>
            </div>
          </div>
          <div className="row">
            <div className="col-xs-12 col-sm-6 col-lg-4">
              <button className="button-primary" onClick={handleUploadDocument}>Register field</button>
            </div>
          </div>
          <div className="row">
            <div className="col-xs-12 col-sm-6 col-lg-4">
              <NavLink to="/management-file-generator">
                <button className="button-primary">Register event to management file</button>
              </NavLink>
            </div>
          </div>
        </div>
      </div>
    )
}

export default Manage
import React, {useState, useEffect} from 'react'
import styles from './Simulations.module.css'
import MenuBar from './../../components/MenuBar/MenuBar'
import SetupSimulation from '../../components/SetupSimulation/SetupSimulation'

import { get, isSignedIn } from '../../services'
import { useHistory } from 'react-router-dom'
import { fieldsEndpoint, simulationsEndpoint } from '../../endpoints'

const Simulations = () => {

    const [fields, setFields] = useState([])
    const [simulations, setSims] = useState([])
    const history = useHistory()

    if(!isSignedIn()) {
      history.replace('/')
    }

    useEffect(() => {
        get(fieldsEndpoint + `?uid=${localStorage.getItem('uid')}`).then(fields => {
            setFields(fields)
        })
        .catch(err => console.log(err))

        get(simulationsEndpoint + `?uid=${localStorage.getItem('uid')}`).then(simulations => {
            setSims(simulations)
        })
        .catch(err => console.log(err))
    }, [])

    return(
        <div className={styles.Simulations}>
            <MenuBar />
            <div className="container">
                <div className="row">
                    <div className="col-xs-12">
                        <h1 className="title">Simulations</h1>
                    </div>
                </div>
            </div>
            <SetupSimulation
                simulations={simulations}
                fields={fields}
            />
        </div>
    )
}

export default Simulations
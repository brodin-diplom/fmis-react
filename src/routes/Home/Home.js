import React from 'react';
import styles from './Home.module.css'
import firebase from 'firebase/app'
import 'firebase/auth'
import { useHistory } from 'react-router-dom'

import { post, get } from '../../services'
import { usersEndpoint, usersEndpointWithId } from '../../endpoints'

const Home = () => {

    const history = useHistory()

    const login = () => {
      const email =  document.querySelector('#email')
      const password =  document.querySelector('#password')
      firebase
        .auth()
        .signInWithEmailAndPassword(email.value, password.value)
        .then(res => {
          if(res.user) {
            localStorage.setItem('uid', res.user.uid)
            const response = get(usersEndpointWithId(res.user.uid))
            response.then(json => {
              if(json.status === 404) {
                const payload = {
                  id: res.user.uid,
                  email: email.value
                }
    
                const response = post(usersEndpoint, payload)
                response.then(json => {
                  localStorage.setItem('uid', json.id)
                  history.push('/manage')
                })
              }
            })
            history.push('/manage')
          }
        })
        .catch(err => {
          const error = err.code

          switch (error) {
            case "auth/email-already-in-use":
              alert('Email already in use')
              break
            case "auth/wrong-password":
              alert('Wrong password')
              break
            case "auth/user-not-found":
              alert('No user found with this email')
              break
            default:
              alert('Something went wrong\nCheck your email and password and try again')
              break
          }
        })
    }

    const signup = () => {
      const email =  document.querySelector('#email')
      const password =  document.querySelector('#password')

      firebase
        .auth()
        .createUserWithEmailAndPassword(email.value, password.value)
        .then(res => {
          if(res.user) {
            const payload = {
              id: res.user.uid,
              email: email.value
            }

            const response = post(usersEndpoint, payload)
            response.then(json => {
              localStorage.setItem('uid', json.id)
              history.push('/manage')
            })
            .catch(err => {
              alert(err)
            })
          }
        })
        .catch(err => {
          const error = err.code

          switch (error) {
            case "auth/email-already-in-use":
              alert('Email already in use')
              break
            case "auth/weak-password":
              alert('Choose a password with at least 6 characters')
              break
            case "auth/invalid-email":
              alert('Enter a valid email')
              break
            default:
              alert('Something went wrong')
              break
          }
        })
    }
  
    return(
      <div className={styles.Home}>
        <div className="container">
          <div className="row center-xs">
            <div className="col-xs-12">
              <h1 className="title">Farm Management Information System</h1>
            </div>
          </div>
            <div className="row center-xs">
                <div className="col-xs-12 col-sm-6 col-lg-5">
                  <input id="email" type="email" className="standard-input" placeholder="Email"/>
                </div>
            </div>
            <div className="row center-xs">
                <div className="col-xs-12 col-sm-6 col-lg-5">
                  <input id="password" type="password" className="standard-input" placeholder="Password"/>
                </div>
            </div>
            <div className="row center-xs">
                <div className="col-xs-12 col-sm-6 col-lg-3">
                  <button onClick={login} className="button-primary">Login</button>
                </div>
            </div>
            <div className="row center-xs">
                <div className="col-xs-12 col-sm-6 col-lg-3">
                  <button style={{"marginTop": "10px"}} onClick={signup} className="button-primary">Sign up</button>
                </div>
            </div>
        </div>
      </div>
    )
}

export default Home
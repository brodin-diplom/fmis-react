import React, { useState, useEffect } from "react"
import styles from "./CreateManagementFile.module.css"
import MenuBar from './../../components/MenuBar/MenuBar'
import FileGenerator from './../../components/FileGenerator/FileGenerator'
import { isSignedIn } from '../../services'
import { useHistory } from 'react-router-dom'

import { get, getUid } from '../../services'
import { fieldsEndpoint } from '../../endpoints'

const CreateManagementFile = () => {
  const [fields, setFields] = useState([])
  const history = useHistory()

  if(!isSignedIn()) {
    history.replace('/')
  }

  useEffect(() => {
    get(fieldsEndpoint+`?uid=${getUid()}`).then(fields => {
      setFields(fields)
    })
    .catch(err => console.log(err))
  }, [])

  return (
    <div className={styles.CreateManagementFile}>
      <MenuBar />
      <div className="container">
        <div className="row">
          <div className="col-xs-12">
            <h1 className="title">Register event</h1>
          </div>
        </div>
        <FileGenerator
          fields={fields}
        />
      </div>
    </div>
  )
}

export default CreateManagementFile;
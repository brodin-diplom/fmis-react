export const get = async (url) => {
    const res = await fetch(url)
    return res.json()
}

export const post = async (url, payload) => {
    const res = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Headers': '*'
        },
        body: JSON.stringify(payload)
    })

    return res.json()
}

export const put = async (url, payload) => {
    fetch(url, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Headers': '*'
        },
        body: JSON.stringify(payload)
    })
}

export const isSignedIn = () => {
    return localStorage.getItem('uid') !== null
}

export const getUid = () => {
    return localStorage.getItem('uid')
}

export const areSameDates = (first, second) => {
    return first.getFullYear() === second.getFullYear() &&
    first.getMonth() === second.getMonth() &&
    first.getDate() === second.getDate()
}
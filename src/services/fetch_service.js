export const get = (url) => {
  fetch(url).then(response => {
    if(!response.ok) throw Error(response.statusText)
    return response.json()
  })
}
import React, { useRef } from "react"
import styles from "./ZoneInfo.module.css"
import { NavLink } from "react-router-dom"

import Select from 'react-select'

const ZoneInfo = (props) =>  {
  const options = useRef([])

  options.current = props.defactionsOption

  const {
    zoneNumber,
    onDefactionClicked,
  } = props

  return(
    <div className={styles.ZoneInfo}>
      <div className="container">
        <div className="row">
          <div className="col-xs-12"> 
            <div className={styles.YearAndZone}>
              <h2 className={styles.Zone}>Zone {zoneNumber}</h2>
              <div className={styles.DefactionSelectWrapper}>
                <Select
                  defaultValue={options.current.filter(opt => opt.value === props.defactionsOption[0].value)}
                  options={options.current}
                  onChange={(selected) => {
                    onDefactionClicked(selected.value)
                  }}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-xs-12 col-sm-8 col-lg-12">
          <div className={styles.ButtonContainer}>
            <NavLink to="/management-file-generator">
              <button className="button-primary">Add event to zone {zoneNumber}</button>
            </NavLink>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ZoneInfo;
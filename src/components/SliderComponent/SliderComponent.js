import React, { useState } from 'react'
import styles from './SliderComponent.module.css'
import Slider from 'rc-slider'
import "rc-slider/assets/index.css"

const SliderComponent = (props) => {

    const [value, setValue] = useState(1)

    return (
        <div className={styles.SliderComponent}>
            <Slider
                min={1}
                max={props.max}
                value={value}
                onChange={(value) => {
                    setValue(value)
                    props.onChange(value)
                }}
                railStyle={{
                    height: 8,
                    backgroundColor: "#DFE0DF"
                }}
                handleStyle={{
                    position: "relative",
                    height: 18,
                    width: 18,
                    backgroundColor: "#2D2D2D",
                    border: "none"
                }}
                trackStyle={{
                    height: 8,
                    background: "#63BAAA"
                }}
                activeDotStyle={{
                    boxShadow: "none"
                }}
            />
            <input className={styles.NumberInput} min={1} max={props.max}  maxLength={1} type="number" value={value} onChange={(e) => {
                if(e.target.value > props.max) {
                    setValue(props.max)
                    props.onChange(props.max)
                } else if(e.target.value < 1) {
                    setValue(1)
                    props.onChange(1)
                } else {
                    setValue(e.target.value)
                    props.onChange(parseInt(e.target.value))
                }
            }}/>
        </div>
    )
}

export default SliderComponent
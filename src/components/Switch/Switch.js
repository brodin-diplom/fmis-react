import React from 'react'
import styles from './Switch.module.css'

const Switch = (props) => {


  return(
    <div className={styles.SwitchWrapper}>
      <label className={styles.Switch}>
          <input type="checkbox" onChange={(e) => props.onChange(e.target)}/>
          <span className={styles.RoundNode}/>
      </label>
      <h5>{props.text}</h5>
    </div>
  )
}

export default Switch
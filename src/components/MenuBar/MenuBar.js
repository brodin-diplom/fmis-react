import React, { useState } from "react"
import styles from "./MenuBar.module.css"
import { NavLink, useHistory } from "react-router-dom"

import firebase from 'firebase/app'
import 'firebase/auth'


const MenuBar = () => {

    const [isMenuOpen, setShowMenu] = useState(false)
    const history = useHistory()

    const handleBurgerClicked = () => setShowMenu(!isMenuOpen)

    const handleLogout = () => {
        firebase
            .auth()
            .signOut()
            .then(() => {
                localStorage.clear()
                history.replace('/')
            })
            .catch(err => {
                console.log(err)
            })
    }

    return(
        <div className={styles.MenuBarComponent}>
            <div className={styles.BurgerWrapper} onClick={handleBurgerClicked}>
                <div className={isMenuOpen ? styles.BurgerOpen : styles.BurgerClose}>
                    <div className={styles.Bar}/>
                    <div className={styles.Bar}/>
                    <div className={styles.Bar}/>
                </div>
            </div> 
            <div className="row">
                <div className={isMenuOpen ? styles.LogoutButtonShow : styles.LogoutButtonHide}>
                    <button onClick={handleLogout}>Log out</button>
                </div>
                <div className={isMenuOpen ? styles.MenuBarShow : styles.MenuBarHide}>
                    <NavLink 
                        to="/"
                        className={styles.Logo}>
                            FMIS
                    </NavLink>
                <div className={isMenuOpen ? styles.MenuesShow : styles.MenuesHide}>
                    <NavLink
                        exact to="/manage" 
                        activeStyle={{
                            opacity: 1
                        }} 
                        className={styles.Item}>
                            Manage
                    </NavLink>

                    <NavLink
                        exact to="/fields"
                        activeStyle={{
                            opacity: 1
                        }} 
                        className={styles.Item}>
                            Fields
                    </NavLink>

                    <NavLink 
                        exact to="/simulations"
                        activeStyle={{
                            opacity: 1
                        }} 
                        className={styles.Item}>
                            Simulations
                        </NavLink>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default MenuBar
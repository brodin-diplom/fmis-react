import React, {Component} from "react"
import styles from "./Map.module.css"
import Timeline from "../Timeline/Timeline"
import ZoneInfo from "../ZoneInfo/ZoneInfo"

import { get } from '../../services'
import { fieldsEndpointWithId } from '../../endpoints'

class Map extends Component {
  constructor(props) {
    super(props)

    this.handleClick = this.handleClick.bind(this)
    
    this.state = {
      defactionIdx: 0,
      defactionsOption: [],
      zoneData: null
    }

    this.fieldId = localStorage.getItem('fieldId')
  }
  
  componentDidMount() {
    const mapSrc = "./js/map.js"
    const zoneNumber = localStorage.getItem('zone-number')
    const options = []
    
    const mapScript = document.createElement("script")
    mapScript.src = mapSrc
    mapScript.setAttribute("id", "map-script")
    document.body.appendChild(mapScript)

    if(this.fieldId !== null && zoneNumber !== null) {
      get(fieldsEndpointWithId(this.fieldId) +  `/zones/${zoneNumber}`).then(zone => {
          this.setState({
            zoneData: zone
          })
          zone.defactions.forEach((defaction, idx) => {
            options.push({
              value: idx,
              label: defaction.name
            })
          })
        })
        .catch(err => {
          this.setState({
            zoneData: null
          })
        })
  
      this.setState({
        defactionsOption: options
      })
    }
    
  }

  componentWillUnmount() {
    document.querySelector("#map-script").remove()
  }
    
  handleClick() {
    const zoneNumber = localStorage.getItem('zone-number')
    this.setState({
      defactionIdx: 0
    })

    get(fieldsEndpointWithId(this.fieldId) +  `/zones/${zoneNumber}`).then(json => {
      this.setState({
        zoneData: json
      })
    })
    .catch(err => {
      this.setState({
        zoneData: null
      })
    })
    
    const options = []
    get(fieldsEndpointWithId(this.fieldId) + `/zones/${zoneNumber}`).then(zone => {
      zone.defactions.forEach((defaction, idx) => {
        options.push({
          value: idx,
          label: defaction.name
        })
      });
    })
    .catch(err => {
      console.log(err)
    })

    this.setState({
      defactionsOption: options
    })
  }

  handleOnDefactionClicked(defIdx) {
    const {
      zoneData
    } = this.state

    if(zoneData !== null && zoneData.defactions.length !== 0) {
      this.setState({
        defactionIdx: defIdx
      })
    }
  }

  render() {
    let content = null
    const {
      zoneData,
      defactionIdx,
      defactionsOption
    } = this.state

    if(zoneData !== null ) {
      content = <div className={styles.TimelineWrapper}>
                  <div className="container">
                    { zoneData.defactions.length !== 0 &&
                      <div className="row">
                        <div className="col-xs-12">
                          <a  className={styles.DownloadLink} href={fieldsEndpointWithId(this.fieldId) + `/zones/${zoneData.id}/get-zone-file`}>Download Management File</a>
                        </div>
                      </div>
                    }
                    <div className="row">
                      <div className="col-xs-12 col-lg-6">
                        <ZoneInfo
                          year={zoneData.defactions[defactionIdx] !== undefined ? new Date(zoneData.defactions[defactionIdx].events[0].date).getFullYear() : "----"}
                          zoneNumber={zoneData.id}
                          onDefactionClicked={(defIdx) => this.handleOnDefactionClicked(defIdx)}
                          defactionsOption={defactionsOption}
                        />
                      </div>
                      <div className="col-xs-12 col-lg-6">
                        { zoneData.defactions[0] !== undefined &&
                            <Timeline
                              date={zoneData.defactions[defactionIdx].events[0].date}
                              type={zoneData.defactions[defactionIdx].name}
                              events={zoneData.defactions[defactionIdx].events}
                            />
                        }
                      </div>
                    </div>
                  </div>
                </div>
    } else {
      content = <p>No data to show</p>
    }

    return (
      <div className={styles.Map}>
        <div className="row">
          <div className="col-xs-12">
            <h1 className={styles.FieldName} id="field-name"> </h1>
            <div id="map" className={styles.MapContainer} onClick={this.handleClick}>
              <div className="row center-xs">
                <div style={{"marginBottom": "10px"}} className="col-xs-12 col-lg-6">
                  <button className="button-primary" onClick={() => document.location.reload()}>Load map</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          {content}  
        </div>
      </div>
    );
  }
}

export default Map;
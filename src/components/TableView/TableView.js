import React from 'react'
import styles from './TableView.module.css'

const TableView = (props) => {
    const results = props.simulationResult

    return (
        <div className={styles.TableView}>
            <h1 className="title">{"Simulation: "}{props.fieldName}</h1>
            <div className={styles.TableWrapper}>
                <table>
                    <tbody>
                        <tr>
                            <th>Year</th>
                            <th>Month</th>
                            <th>Day</th>
                            <th>Stem DM</th>
                            <th>Dead DM</th>
                            <th>Leaf DM</th>
                            <th>Sorg DM</th>
                            <th>Stem N</th>
                            <th>Dead N</th>
                            <th>Leaf N</th>
                            <th>Sorg N</th>
                            <th>W Stress</th>
                            <th>N Stress</th>
                            <th>WP ET</th>
                            <th>HI</th>
                        </tr>
                    { 
                        results.map((sim, idx) => {
                            return <tr key={idx}>
                                <td> {sim.year} </td>
                                <td> {sim.month} </td>
                                <td> {sim.day} </td>
                                <td> {sim.stem_DM}<br/> <span className={styles.Units}>Mg DM/ha</span></td>
                                <td> {sim.dead_DM}<br/> <span className={styles.Units}>Mg DM/ha</span></td>
                                <td> {sim.leaf_DM}<br/> <span className={styles.Units}>Mg DM/ha</span></td>
                                <td> {sim.sorg_DM}<br/> <span className={styles.Units}>Mg DM/ha</span></td>
                                <td> {sim.stem_N}<br/> <span className={styles.Units}>kg N/ha</span></td>
                                <td> {sim.dead_N}<br/> <span className={styles.Units}>kg N/ha</span></td>
                                <td> {sim.leaf_N}<br/> <span className={styles.Units}>kg N/ha</span></td>
                                <td> {sim.sorg_N}<br/> <span className={styles.Units}>kg N/ha</span></td>
                                <td> {sim.WStress} <span className={styles.Units}>d</span></td>
                                <td> {sim.NStress} <span className={styles.Units}>d</span></td>
                                <td> {sim.WP_ET}<br/> <span className={styles.Units}>kg/m^3</span></td>
                                <td> {sim.HI} </td>
                            </tr>
                        })
                    }
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default TableView
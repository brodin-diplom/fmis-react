import React, {Component} from "react"
import styles from "./Timeline.module.css"

import { areSameDates } from '../../services'

class Timeline extends Component {
  constructor(props) {
    super(props)

    this.inviewAnimate = this.inviewAnimate.bind(this)
  }

  componentDidMount() {
    const animationElements = [...document.querySelectorAll('[data-animate]')]
    this.inviewAnimate(animationElements)
    document.addEventListener('scroll', () => {
      this.inviewAnimate(animationElements)
    })
  }

  componentDidUpdate() {
    const animationElements = [...document.querySelectorAll('[data-animate]')]
    this.inviewAnimate(animationElements)
    document.addEventListener('scroll', () => {
      this.inviewAnimate(animationElements)
    })
  }
  
  inviewAnimate(animationElements) {
    animationElements.forEach(element => {
      if(this.isInViewport(element) && element.getAttribute('data-animate') === "inview-point") {
        element.setAttribute('data-animate', 'inview-animate-point')
      } else if(this.isInViewport(element) && element.getAttribute('data-animate') === "inview-text") {
        element.setAttribute('data-animate', 'inview-animate-text')
      }
    });
  }

  isInViewport(element) {
    const rect = element.getBoundingClientRect();
    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
  }

  render() {
    const {
      type,
      events
    } = this.props

    const sortedEvents = events.slice().sort((eventA, eventB) => new Date(eventA.date) - new Date(eventB.date))
    let array = []
    let arrayRunningTo

    if(sortedEvents.length  > 1) {
      arrayRunningTo = sortedEvents.length-1
    } else {
      arrayRunningTo = sortedEvents.length
    }
    for(let i = 0; i < arrayRunningTo; i++) {
      if(sortedEvents.length > 1 && areSameDates(new Date(sortedEvents[i+1].date), new Date(sortedEvents[i].date))) {
        const date = new Date(sortedEvents[i+1].date);
        const keyName = date.getDate() + "/" + (date.getMonth()+1) + "/" + date.getFullYear()
        sortedEvents[keyName] = array
        sortedEvents[keyName].push(sortedEvents[i])
        sortedEvents[keyName].push(sortedEvents[i+1])
      } else {
        let date
        if(sortedEvents.length > 2) {
          date = new Date(sortedEvents[i+1].date);
          const keyName = date.getDate() + "/" + (date.getMonth()+1) + "/" + date.getFullYear()
          sortedEvents[keyName] = sortedEvents[i+1]
        } else {
          date = new Date(sortedEvents[i].date);
          const keyName = date.getDate() + "/" + (date.getMonth()+1) + "/" + date.getFullYear()
          sortedEvents[keyName] = sortedEvents[i]
        }
        array = []
      }
    }

    const singleDatesArray = []
    events.forEach(event => {
      if(!singleDatesArray.includes(event.date)) {
        singleDatesArray.push(event.date)
      }
    })

    const lastArray = []
    const idArray = []

    singleDatesArray.forEach(el => {
      const date = new Date(el);
      const keyName = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear()
      if(Array.isArray(sortedEvents[keyName])) {
        const cleanedArr = [...new Set(sortedEvents[keyName])]
        lastArray.push([...new Set(sortedEvents[keyName])])
        cleanedArr.forEach(el => {
          idArray.push(el.id)
        });
      } else {
        if(sortedEvents[keyName] !== undefined && !idArray.includes(sortedEvents[keyName].id)) {
          lastArray.push(sortedEvents[keyName])
          idArray.push(sortedEvents[keyName].id)
        }        
      }
    });
    sortedEvents.forEach(el => {
      if(!idArray.includes(el.id)) {
        lastArray.push(el)
      }
    });

    return(
      <div className={styles.Timeline}>
        <div className={styles.Line}/>
        <div className={styles.TimelineEvents}>
          <div className={styles.PointContainer}>
            <div className={styles.StartPoint} data-animate="inview-point"/>
            <div className={styles.StartPointText} data-animate="inview-text">
              {type.replace(/_/g, ' ')}
            </div>
          </div>
          { lastArray.map((event, i) => {
            let eventDate

            if(Array.isArray(event)) {
              eventDate = new Date(event[0].date)
            } else {
              eventDate = new Date(event.date)
            }

            const day = eventDate.getDate()
            const month = eventDate.getMonth() + 1
            const year = eventDate.getFullYear()

            return (
              <div key={i} className={styles.Event}>
                <div className={styles.SubPointContainer}>
                  <div className={styles.SubPoint} data-animate="inview-point"/>
                  <div className={styles.SubPointText} data-animate="inview-text">
                    {
                      day + "/" + month + '/' + year
                    }
                  </div>
                </div>
                <div>
                  { Array.isArray(event) && event.map((linkedEvent,j) => {
                    let eventText = ""

                    if(linkedEvent.type === "fertilize") {
                      eventText = (linkedEvent.type.charAt(0).toUpperCase() + linkedEvent.type.slice(1) + " " + linkedEvent.nitrogen_level + " Kg N/Ha").replace(/_/g, ' ')
                    } else if(linkedEvent.type === "sow" || linkedEvent.type === "harvest") {
                      eventText = (linkedEvent.type.charAt(0).toUpperCase() + linkedEvent.type.slice(1) + " " + linkedEvent.crop_type).replace(/_/g, ' ')
                    } else {
                      eventText = (linkedEvent.type.charAt(0).toUpperCase() + linkedEvent.type.slice(1)).replace(/_/g, ' ')
                    }

                    return(
                      <div key={j} className={styles.SubSubPointContainer}>
                        <div className={styles.SubSubPoint} data-animate="inview-point"/>
                        <div className={styles.SubSubPointText} data-animate="inview-text">
                          {eventText}
                        </div>
                      </div>
                    )
                  })
                  }
                  { event.type === "fertilize" && !Array.isArray(event) &&
                  <div className={styles.SubSubPointContainer}>
                    <div className={styles.SubSubPoint} data-animate="inview-point"/>
                    <div className={styles.SubSubPointText} data-animate="inview-text">
                      {(event.type.charAt(0).toUpperCase() + event.type.slice(1)).replace(/_/g, ' ')} {event.nitrogen_level} Kg N/Ha
                    </div>
                  </div>
                }
                { event.type === "sow" && !Array.isArray(event) &&
                  <div className={styles.SubSubPointContainer}>
                    <div className={styles.SubSubPoint} data-animate="inview-point"/>
                    <div className={styles.SubSubPointText} data-animate="inview-text">
                      {(event.type.charAt(0).toUpperCase() + event.type.slice(1)).replace(/_/g, ' ')} {event.crop_type.replace(/_/g, ' ')}
                    </div>
                  </div>
                }
                { event.type === "harvest" && !Array.isArray(event) &&
                  <div className={styles.SubSubPointContainer}>
                    <div className={styles.SubSubPoint} data-animate="inview-point"/>
                    <div className={styles.SubSubPointText} data-animate="inview-text">
                      {(event.type.charAt(0).toUpperCase() + event.type.slice(1)).replace(/_/g, ' ')} {event.crop_type.replace(/_/g, ' ')}
                    </div>
                  </div>
                }
                { event.type !== "harvest" && event.type !== "sow" && event.type !== "fertilize" && !Array.isArray(event) &&
                  <div className={styles.SubSubPointContainer}>
                    <div className={styles.SubSubPoint} data-animate="inview-point"/>
                    <div className={styles.SubSubPointText} data-animate="inview-text">
                      {(event.type.charAt(0).toUpperCase() + event.type.slice(1)).replace(/_/g, ' ')}
                    </div>
                  </div>
                }
                </div>
              </div>
            )})
          }
        </div>
      </div>
    )
  }
}

export default Timeline
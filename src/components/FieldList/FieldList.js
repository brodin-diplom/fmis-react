import React, {Component} from "react"
import styles from "./FieldList.module.css"
import {
  isMobile
} from "react-device-detect";

class FieldList extends Component {
  constructor(props) {
    super(props)
    this.toggleList = this.toggleList.bind(this)

    this.fieldIndex = localStorage.getItem('fieldIndex') !== null ? localStorage.getItem('fieldIndex') : 0

    this.state = {
      fieldIndex: this.fieldIndex,
      showList: true,
      fieldId: this.props.fields[this.fieldIndex].id,
      kmlSrc: this.props.fields[this.fieldIndex].kmlUrl
    }
  }
  
  componentDidMount() {
    if(isMobile) this.setState({ showList: false })
    document.getElementById("field-name").innerText = this.props.fields[this.state.fieldIndex].name
    if(localStorage.getItem('fieldId') === null) {
      const fieldId = this.props.fields[this.state.fieldIndex].id
      this.setState({
        fieldId: fieldId
      })
    }
  }

  componentDidUpdate() {
    localStorage.setItem('fieldId', this.state.fieldId)
    localStorage.setItem('fieldIndex', this.state.fieldIndex)
    localStorage.setItem('kmlSrc', this.state.kmlSrc)
  }
  
  chooseField(index) {
    const fieldId = this.props.fields[index].id
    this.setState({
      fieldId: fieldId,
      fieldIndex: index,
      kmlSrc: this.props.fields[index].kmlUrl
    })
    document.location.reload()
  }

  toggleList() {
    this.setState({
      showList: !this.state.showList
    })
  }

  render() {
    const {
      showList
    } = this.state
    
    const {
      fields
    } = this.props


    return (
      <div className={styles.FieldListComponentWrapper}>
        <div className={styles.MobileSelector} onClick={this.toggleList}>
          <button className="button-primary">Fields</button>
        </div>
          <div className={showList ? styles.FieldListShow : styles.FieldListHide} id="field-list">
              <div className="row">
                <div className={styles.TitleWithButton}>
                  <h2>Fields</h2>
                </div>
              </div>
              { fields.length !== 0 &&
              <div className="row">
                {
                  fields.map((field, index) => {
                    let element = null

                    if(index === parseInt(localStorage.getItem('fieldIndex'))) {
                      element = <div key={index} className="col-xs-4 col-sm-3 col-lg-12">
                                  <p className={showList ? styles.FieldHighlight : styles.FieldHighlightHidden} onClick={() => this.chooseField(index)}>{field.name}</p>
                                </div>
                    } else {
                      element = <div key={index} className="col-xs-4 col-sm-3 col-lg-12">
                                  <p className={showList ? styles.FieldNormal : styles.FieldNormalHidden} onClick={() => this.chooseField(index)}>{field.name}</p>
                                </div>
                    }
                    return element
                  })
                }
              </div>
              }
          </div>
      </div>
    );
  }
}

export default FieldList;
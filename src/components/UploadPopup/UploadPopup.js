import React, { useEffect, useRef, useState } from 'react';
import styles from './UploadPopup.module.css'
import firebase from 'firebase/app'
import 'firebase/storage'

import { post } from '../../services'
import { fieldsEndpoint } from '../../endpoints'

const UploadPopup = (props) => {
    let files = useRef([])
    const [showProgress, setShowProgress] = useState(false)
    
    useEffect(()=> {
        document.querySelector('#kml-file').addEventListener('change', (e) => {
            files.current = e.target.files
        })
    }, [])
    
    const uploadToFirebaseStorage = () => {
        const nameField = document.querySelector('#field-name')

        if(files.current.length !== 0 && nameField.value !== "") {
            for(let i = 0; i < files.current.length; i++) {
                const id = localStorage.getItem('uid')
                const storageRef = firebase.storage().ref(id + files.current[i].name)
                const upload = storageRef.put(files.current[i])

                upload.on(
                    "state_changed",
                    function progress(snapshot) {
                        setShowProgress(true)
                        let percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                        document.querySelector("#progress-text").innerHTML = percentage + "%";
                    },
          
                    function error() {
                      alert("Error uploading file");
                    },
          
                    function complete() {
                        setShowProgress(false)
                        storageRef.getDownloadURL().then(url => {
                            const payload = {
                                uid: localStorage.getItem('uid'),
                                name: nameField.value,
                                kmlUrl: url
                            }
                            
                            const response = post(fieldsEndpoint, payload)
                            response.then(json => {
                                props.dismiss()
                                nameField.value = ""
                                document.querySelector('#kml-file').value = null
                            })
                        })
                    }
                  );
            }
        } else {
            alert("Choose a kml file and a name")
        }
    }

    return(
        <div className={styles.Popup}>
            <div 
                onClick={props.dismiss} 
                className={props.shouldShowPopup ? styles.Backdrop : styles.BackdropHidden}
            />
            <div className={props.shouldShowPopup ? styles.UploadPopupShow : styles.UploadPopupHide}>
                <h2 className={styles.SubTitle}>Create Field</h2>
                <div className={showProgress ? styles.ProgressShow : styles.ProgressHide}>
                    <div className={styles.ProgressBackdrop}/>
                    <div className={styles.ProgressWrapper}>
                        <h3 className={styles.Progress} id="progress-text"> 100% </h3>
                    </div>
                </div>
                <div className={styles.Close} onClick={props.dismiss}>
                    <svg width="42" height="42" viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M6.7829 6.7829L35.0672 35.0672" stroke="#2D2D2D" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round"/>
                        <path d="M6.7829 35.0672L35.0672 6.7829" stroke="#2D2D2D" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round"/>
                    </svg>
                </div>
                <label style={{"color": "var(--colors-primary)"}}>Upload KML file</label>
                <input className={styles.FileUpload} type="file" id="kml-file" name="kml-file" accept=".kml"/>

                <label style={{"color": "var(--colors-primary)"}}>Name field</label>
                <div className="row">
                    <div className="col-xs-12 col-lg-6">
                        <input className="standard-input" type="text" id="field-name" name="field-name"/>
                    </div>
                </div>

                <div className={styles.SubmitButton}>
                    <div className="row center-xs">
                        <div className="col-xs-12 col-lg-11">
                            <button onClick={uploadToFirebaseStorage} className="button-primary">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default UploadPopup
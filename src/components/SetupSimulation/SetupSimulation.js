import React, { useRef, useState } from 'react'
import styles from './SetupSimulation.module.css'
import Select from 'react-select'
import SliderComponent from '../SliderComponent/SliderComponent'
import TableView from '../TableView/TableView'
import Switch from '../Switch/Switch'
import Lottie from 'react-lottie'
import loadingAnim from './loader'

import { fieldsEndpointWithId, simulationsEndpointWithId } from '../../endpoints'
import { get, post } from '../../services'

const SetupSimulation = (props) => {
    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: loadingAnim,
        rendererSettings: {
          preserveAspectRatio: "xMidYMid slice"
        }
    };

    const [simIsRunning, setSimIsRunning] = useState(false)
    const options = useRef([])
    const simulations = useRef([])
    const zonesOptions = useRef([])
    const simulatedZonesOption = useRef([])
    const [fieldName, setFieldName] = useState("")
    const [fieldId, setFieldId] = useState("")
    const [zones, setZones] = useState([])
    const [sim, setSim] = useState(null)
    const [zonesArray, setZonesArray] = useState([])
    const [simResult, setSimResult] = useState(null)
    const [allSimResults, setAllSimResults] = useState(null)
    const [differentValues, setDifferentValues] = useState(false)
    
    if(options.current.length === 0) {
        props.fields.forEach(field => {
            options.current.push({
                value: field.id,
                label: field.name
            })
        })
    }

    if(simulations.current.length === 0) {
        props.simulations.forEach(sim => {
            simulations.current.push({
                value: sim.id,
                label: sim.name
            })
        })
    }

    const handleChangedInputFields = (selected) => {
        const selectedId = selected.value
        setFieldName(selected.label)
        setFieldId(selectedId)
        setSimResult(null)
        setAllSimResults(null)
        setSim(null)
        simulatedZonesOption.current = []

        const zonesOnField = []
        get(fieldsEndpointWithId(selectedId)).then(field => {
            field.zones.forEach(zone => zonesOnField.push({
                zoneId: zone.id,
                nValue: 1
            }))
            setZonesArray(zonesOnField)
            setZones(field.zones)
        })
        .catch(err => console.log(err))
        
    }
    
    const handleChangedInputSims = (selected) => {
        const selectedId = selected.value
        setZones([])
        setFieldName("")
        
        get(simulationsEndpointWithId(selectedId)).then(simulation => {
            setSim(simulation)

            if(zonesOptions.current.length === 0) {
                simulation.simulations.forEach((sim, idx) => {
                    zonesOptions.current.push({
                        value: sim.zone_id,
                        label: sim.zone_id
                    })
                })
            }
        })
    }
    
    const handleChangedInputZones = (selected) => {
        const selectedId = selected.value
        setZones([])
        setFieldName("")
        
        sim.simulations.forEach(result => {
            if(result.zone_id === selectedId) {
                setSimResult(result.results)
            }
        })
    }

    const handleZoneNLevelChanged = (v, zoneId) => {
        const zonesOnField = []
        zonesArray.forEach((zone, idx) => {
            if(zone.zoneId === zoneId) {
                zonesOnField.push({
                    zoneId: zoneId,
                    nValue: v
                })
            } else {
                zonesOnField.push({
                    zoneId: zone.zoneId,
                    nValue: zonesArray[idx].nValue
                })
            }
        })
        setZonesArray(zonesOnField)
    }

    const handleNLevelChanged = (v) => {
        const zonesOnField = []
        zonesArray.forEach( zone => {
            zonesOnField.push({
                zoneId: zone.zoneId,
                nValue: v
            })
        })
        setZonesArray(zonesOnField)
    }
    
    const handleSwitchChanged = (element) => {
        setDifferentValues(element.checked)
        const zonesOnField = []
        zonesArray.forEach( zone => {
            zonesOnField.push({
                zoneId: zone.zoneId,
                nValue: 1
            })
        })
        setZonesArray(zonesOnField)
    }

    const handleSimulatedZones = (selected) => {
        setSimResult(allSimResults[selected.value].results)
    }

    const runSimulation = () => {
        setSimIsRunning(true)
        const payload = {
            nitrogenValues: zonesArray
        }

        post(fieldsEndpointWithId(fieldId) + `/runSimulation`, payload).then(simResults => {
            simResults.results.forEach((sim, idx) => {
                simulatedZonesOption.current.push({
                    value: idx,
                    label: sim.zone_id
                })
            })
            setAllSimResults(simResults.results)
            setSimResult(simResults.results[0].results)
            setSimIsRunning(false)
        })
        .catch(_ => {
            setSimIsRunning(false)
            alert('Something went wrong during the simulation')
        })
    }

    return(
        <div className={styles.SetupSimulations}>
            <div className="container">
                { simIsRunning &&
                    <div className={styles.LoadingWrapper}>
                        <Lottie 
                            options={defaultOptions}
                            height={400}
                            width={400}
                        />
                    </div>
                }
                <div className="row">
                    <div className="col-xs-12 col-sm-6 col-lg-4">
                        { options.current.length !== 0 &&
                        <div>
                            <h3 className={styles.SubTitle}>Choose a field</h3>
                            <Select
                                options={options.current}
                                onChange={(selected) => handleChangedInputFields(selected)}
                            />
                        </div>
                        }
                        { simulations.current.length !== 0 && !simResult &&
                        <div>
                            <h3 className={styles.SubTitle}>Or choose an existing simulation</h3>
                            <Select
                                options={simulations.current}
                                onChange={(selected) => handleChangedInputSims(selected)}
                            />
                        </div>
                        }
                        {  sim && !simResult &&
                        <div>
                            <h3 className={styles.SubTitle}>Choose a zone</h3>
                            <Select
                                options={zonesOptions.current}
                                onChange={(selected) => handleChangedInputZones(selected)}
                            />
                        </div>
                        }
                    </div>
                </div>

                { zones.length !== 0 && !simResult &&
                    <div className="row">
                        <div className="col-xs-12">
                            <Switch
                                text={"Different nitrogen levels"}
                                onChange={(el) => handleSwitchChanged(el)}
                            />
                        </div>
                    </div>  
                }
                { !simResult &&
                    <h1 className="title">{fieldName}</h1>
                }
                <div className="row">
                    { zones.length !== 0 && differentValues && !simResult &&
                        zones.map((zone, idx) => {
                            return <div key={idx} className="col-xs-12 col-lg-6">
                                    <h4 className={styles.SubTitle}>Nitrogen value for zone {zone.name}</h4>
                                    <SliderComponent 
                                        max={180}
                                        onChange={(value) => handleZoneNLevelChanged(value, zone.name)}
                                    />
                            </div>
                        })
                    }
                    { zones.length !== 0 && !differentValues && !simResult &&
                        <div className="col-xs-12 col-lg-6">
                            <h4 className={styles.SubTitle}>Nitrogen value</h4>
                            <SliderComponent 
                                max={180}
                                onChange={(value) => handleNLevelChanged(value)}
                            />
                        </div>
                    }
                </div>
                { zones.length !== 0 && !simResult &&
                    <div className={styles.ButtonContainer}>
                        <div className="row center-xs">
                            <div className="col-xs-12 col-lg-6">
                                <button className="button-primary" disabled={simIsRunning} onClick={runSimulation}>Run simulation</button>
                            </div>
                        </div>
                    </div>
                }
            </div>
            { simResult !== null &&
                <div>
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-4">
                                <h3 className={styles.SubTitle}>Choose a zone</h3>
                                <Select
                                    options={simulatedZonesOption.current}
                                    onChange={(selected) => handleSimulatedZones(selected)}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-xs-12">
                                <TableView
                                    fieldName={fieldName}
                                    simulationResult={simResult}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            }
        </div>
    )
}

export default SetupSimulation
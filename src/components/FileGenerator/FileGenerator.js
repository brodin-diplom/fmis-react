import React, { useEffect, useRef, useState } from "react"
import styles from "./FileGenerator.module.css"

import DatePicker from 'react-date-picker'
import Select from 'react-select'
import SliderComponent from '../SliderComponent/SliderComponent'
import Switch from '../Switch/Switch'

import { get, put } from "../../services"
import { fieldsEndpoint, eventTypesEndpoint } from '../../endpoints' 

const FileGenerator = (props) => {
  const fieldsOptions = useRef([])
  const zonesOptions = useRef([])
  const defactionOptions = useRef([])
  const eventOptions = useRef([])
  const fertilizeOptions = useRef([])
  const cropOptions = useRef([])
  const [useNewDefaction, setUseNewDefaction] = useState(false)
  const [eventField, setEventField] = useState(null)
  const [eventZone, setEventZone] = useState(null)
  const [eventDefaction, setEventDefaction] = useState(null)
  const [eventType, setEventType] = useState(null)
  const [eventFertilizeType, setFertilizeType] = useState("")
  const [eventNitrogenLevel, setNitrogenLevel] = useState(0)
  const [eventCropType, setCropType] = useState("")
  const [willFertilize, setWillFertilize] = useState(false)
  const [willSowHarvest, setWillSowHarvest] = useState(false)
  const [eventDate, setEventDate] = useState(new Date())

  const addOptions = (url) => {
    get(url).then(data => {
      data.forEach(json => {
        if(json.name === "eventTypes") {
          json.eventTypes.forEach((type, idx) => {
            eventOptions.current.push({
              value: idx,
              label: type
            })
          })
        } else if(json.name === "fertilizationTypes") { 
          json.eventTypes.forEach((type, idx) => {
            fertilizeOptions.current.push({
              value: idx,
              label: type
            })
          })
        } else {
          json.eventTypes.forEach((type, idx) => {
            cropOptions.current.push({
              value: idx,
              label: type
            })
          })
        }
      });
    })
  }

  useEffect(() => {
    addOptions(eventTypesEndpoint)
  }, [])

  if(fieldsOptions.current.length === 0) {
    props.fields.forEach(field => {
      fieldsOptions.current.push({
            value: field.id,
            label: field.name
        })
    })
  }

  const handleChangedInputField = (selected) => {
    const selectedId = selected.value
    setEventField(selectedId)
    setEventZone(null)
    setEventDate(new Date())
    setUseNewDefaction(false)
    zonesOptions.current = []
    defactionOptions.current = []

    get(fieldsEndpoint + `/${selectedId}/zones`).then(zones => {
      zones.forEach((zone) => {
        zonesOptions.current.push({
          value: zone.id,
          label: zone.name
        })
      })
    })
    .catch(err => console.log(err))
  }

  const handleChangedInputZone = (selected) => {
    const selectedId = selected.value
    setEventZone(selectedId)

    get(fieldsEndpoint + `/${eventField}/zones/${selectedId}`).then(zone => {
      zone.defactions.forEach(defaction => {
        defactionOptions.current.push({
          value: defaction.id,
          label: defaction.name
        })
      })
    })
    .catch(err => console.log(err))
  }

  const handleSwitchChanged = (el) => {
    setUseNewDefaction(el.checked)
  }

  const handleChangedInputDefaction = (selected) => {
    if(useNewDefaction){
      setEventDefaction(selected.label)
    } else {
      setEventDefaction(selected.value)
    }
  }

  const handleChangedInputEvent = (selected) => {
    setEventType(selected.label)

    if(selected.label === "fertilize") {
      setWillFertilize(true)
      setWillSowHarvest(false)
    } else if(selected.label === "sow" || selected.label === "harvest") {
      setWillFertilize(false)
      setWillSowHarvest(true)
    } else {
      setWillFertilize(false)
      setWillSowHarvest(false)
    }
  }

  const handleChangedInputFertilize = (selected) => {
    if(willFertilize) setFertilizeType(selected.label)
  }
  
  const handleChangedInputCrop = (selected) => {
    if(willSowHarvest) setCropType(selected.label)
  }

  const handleOnInput = (e) => {
    setEventDefaction(e.target.value)
  }

  const pushPayload = (payload) => {
    put(fieldsEndpoint + `/${eventField}/zones/${eventZone}`, payload).then(data => {
      alert('Event added to file')
      document.location.reload()
    })
  }

  const pushPayloadToDefaction = (payload) => {
    put(fieldsEndpoint + `/${eventField}/zones/${eventZone}/defactions/${eventDefaction}`, payload).then(data => {
      alert('Event added to file')
      document.location.reload()
    })
  }

  const addEvent = () => {
    if(eventField !== null && 
       eventZone !== null &&
       eventDefaction !== null &&
       eventDate !== null &&
       eventType !== null) {

         if(useNewDefaction) {
            const payload = {
              name: eventDefaction,
              event: {
                type: eventType,
                cropType: eventCropType,
                fertilizationType: eventFertilizeType,
                nitrogenLevel: eventNitrogenLevel,
                date: eventDate.toDateString()
              }
            }
            if(willFertilize) {
              if(eventFertilizeType !== "") {
                pushPayload(payload)
              } else {
                alert('Choose a fertilization type')
              }
            } else if(willSowHarvest) {
              if(eventCropType !== "") {
                pushPayload(payload)
              } else {
                alert('Choose a crop type')
              }
            } else {
              pushPayload(payload)
            }
         } else {
            const payload = {
              type: eventType,
              cropType: eventCropType,
              fertilizationType: eventFertilizeType,
              nitrogenLevel: eventNitrogenLevel,
              date: eventDate.toDateString()
            }

            if(willFertilize) {
              if(eventFertilizeType !== "") {
                pushPayloadToDefaction(payload)
              } else {
                alert('Choose a fertilization type')
              }
            } else if(willSowHarvest) {
              if(eventCropType !== "") {
                pushPayloadToDefaction(payload)
              } else {
                alert('Choose a crop type')
              }
            } else {
              pushPayloadToDefaction(payload)
            }
         }
    } else {
      alert("Enter required fields please")
    }
  }

  return(
    <div className={styles.FileGenerator}>
      <div className="row">
        <div className="col-xs-12 col-lg-6">
        { fieldsOptions.current.length !== 0 &&
          <div>
            <h3 className={styles.SubTitle}>1. Choose a field</h3>
            <Select
              options={fieldsOptions.current}
              onChange={(selected) => handleChangedInputField(selected)}
            />
          </div>
        }
        </div>
      </div>
      <div className="row">
        <div className="col-xs-12 col-lg-6">
        { fieldsOptions.current.length !== 0 && eventField !== null && zonesOptions !== null &&
          <div>
            <h3 className={styles.SubTitle}>2. Choose a zone</h3>
            <Select
              options={zonesOptions.current}
              onChange={(selected) => handleChangedInputZone(selected)}
            />
          </div>
        }
        </div>
      </div>
      <div className="row">
        <div className="col-xs-12 col-lg-6">
        { fieldsOptions.current.length !== 0 && eventField !== null && eventZone !== null &&
          <div>
            <h3 className={styles.SubTitle}>3. Add defaction or choose existing</h3>
            <Switch
              className={styles.DefactionSwitch}
              text={"Add new defaction"}
              onChange={(selected) => handleSwitchChanged(selected)}
            />
            { useNewDefaction &&
              <div>
                <input onInput={(e) => handleOnInput(e)} id="new-defaction" className="standard-input" placeholder="Defaction name"/>
              </div>
            }
            { !useNewDefaction &&
              <Select
                options={defactionOptions.current}
                onChange={(selected) => handleChangedInputDefaction(selected)}
              />
            }
          </div>
        }
        </div>
      </div>
      <div className="row">
        <div className="col-xs-12 col-lg-6">
        { fieldsOptions.current.length !== 0 && eventField !== null && eventZone !== null &&
          <div>
            <h3 className={styles.SubTitle}>4. Choose date of event</h3>
            <DatePicker
              maxDate={new Date("08-31-2020")}
              onChange={(date) => {setEventDate(date)}}
              value={eventDate}
              format={"dd-MM-y"}
              required={true}
              className={styles.DatePicker}
            />
          </div>
        }
        </div>
      </div>
      <div className="row">
        <div className="col-xs-12 col-lg-6">
        { eventField !== null && eventZone !== null && eventDate !== null &&
          <div>
            <h3 className={styles.SubTitle}>5. Choose event type</h3>
            <Select
              options={eventOptions.current}
              onChange={(selected) => handleChangedInputEvent(selected)}
            />
            { willFertilize &&
              <div style={{"marginTop": "5px"}}>
                <Select
                  options={fertilizeOptions.current}
                  onChange={(selected) => handleChangedInputFertilize(selected)}
                />
                <h5>Weight in Kg N/Ha</h5>
                <SliderComponent
                  max={180}
                  onChange={(value => {setNitrogenLevel(value)})}
                />
              </div>
            }
            { willSowHarvest && 
              <div style={{"marginTop": "5px"}}>
                <Select
                  options={cropOptions.current}
                  onChange={(selected) => handleChangedInputCrop(selected)}
                />
              </div>
            }
          </div>
        }
        </div>
      </div>
      <div className="row">
        <div className="col-xs-12 col-lg-6">
        { eventField !== null && eventZone !== null && eventDate !== null && !useNewDefaction &&
            <button style={{"margin": "var(--grid-row) 0"}} className="button-primary" onClick={addEvent}>Add defaction and associated event</button>
        }
        { eventField !== null && eventZone !== null && eventDate !== null && useNewDefaction &&
            <button style={{"margin": "var(--grid-row) 0"}} className="button-primary" onClick={addEvent}>Add event to defaction</button>
        }
        </div>
      </div>
    </div>
  )
}

export default FileGenerator
# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

## Flexboxgrid Guide

Guide to use of flexbox grid [https://roylee0704.github.io/react-flexbox-grid/](here)

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `yarn build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)


# --- Dockerbuilds ---
## How to Windows / linux / mac
- docker build -t fmis-react .
- docker run --rm -it -p 80:80 -e fmis-react

## How to deploy on Google Cloud Run
### Cloud run init 
- Make sure you have Storage Object Admin & Stoage Object Creator roles in google cloud: https://console.cloud.google.com/iam-admin/iam?project=nitrogen-sensor-fmis
- Download glcoud: https://cloud.google.com/sdk/docs/install
- Run 'gcloud init', and follow steps
- Run 'gcloud auth configure-docker'

### After first run
- Create the docker image: 
	docker build -f Dockerfile -t gcr.io/nitrogen-sensor-fmis/fmis-react .

- Push the docker image with the tag: 
	docker push gcr.io/nitrogen-sensor-fmis/fmis-react

- Run gcloud deploy: 
	gcloud run deploy fmis-react --project nitrogen-sensor-fmis --image gcr.io/nitrogen-sensor-fmis/fmis-react --allow-unauthenticated --port 80 --cpu 1 --memory 1024Mi --concurrency 80 --timeout 300 --region europe-west6

- Now your instance is up and running
